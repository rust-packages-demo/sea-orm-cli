use futures::executor::block_on;
use sea_orm::*;
// use sea_orm::{ActiveValue, Database, DbErr};

mod entities;
use entities::{prelude::*, *};
// use entity::post::Entity as Post;

// Change this according to your database implementation,
// or supply it as an environment variable.
// the whole database URL string follows the following format:
// "protocol://username:password@host:port/database"
// We put the database name (that last bit) in a separate variable simply for convenience.
const DATABASE_URL: &str = "postgresql://@127.0.0.1/root";
// const DB_NAME: &str = "bakeries_db";

async fn run() -> Result<(), DbErr> {
    let db = Database::connect(DATABASE_URL).await?;

    // Migrator::refresh(db).await?;
    // assert!(schema_manager.has_table("post").await?);

    let happy_post = post::ActiveModel {
        title: ActiveValue::Set("Happiness".to_owned()),
        text: ActiveValue::Set("Happiness is ...".to_owned()),
        ..Default::default()
    };
    let res = Post::insert(happy_post).exec(&db).await?;

    Ok(())
}

fn main() {
    if let Err(err) = block_on(run()) {
        panic!("{}", err);
    }
}
